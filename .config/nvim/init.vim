lua require('tools')
lua require('plugins')
autocmd InsertLeave *.py lua vim.lsp.buf.formatting_sync(nil, 1000)
autocmd BufWritePre *.py lua vim.lsp.buf.formatting_sync(nil, 1000)
