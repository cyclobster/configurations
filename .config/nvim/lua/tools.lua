vim.g.mapleader = ','
vim.g.python3_host_prog = "python"

local key_mapper = function(mode, key, result)
  vim.api.nvim_set_keymap(
    mode,
    key,
    result,
    {noremap = true, silent = true}
  )
end

--Buffer Controls
key_mapper('', '<leader>b', ':bn<cr>')
key_mapper('', '<leader>c', ':bd<cr>')
key_mapper('', '<leader>v', '<c-w>v')
key_mapper('', '<leader>h', '<c-w>s')
key_mapper('', '<c-j>', '<c-w><c-j>')
key_mapper('', '<c-k>', '<c-w><c-k>')
key_mapper('', '<c-h>', '<c-w><c-h>')
key_mapper('', '<c-l>', '<c-w><c-l>')

--Common Tasks
key_mapper('', '<leader>el', ':e ~/.config/nvim/lua/tools.lua<cr>')
key_mapper('', '<leader>ep', ':e ~/.config/nvim/lua/plugins.lua<cr>')
key_mapper('', '<leader>efm', ':e ~/.config/efm-langserver/config.yaml<cr>')
key_mapper('', '<leader>tree', ':NERDTree<cr>')
key_mapper('', '<leader>fb', ':!black --quiet %<cr><cr>')

--CtrlP
key_mapper('', '<c-p>', ':CtrlP<cr>')

--Linters
require "lspconfig".efm.setup{}
