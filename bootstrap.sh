#!/bin/bash
# Go to  https://github.com/neovim/neovim/releases/tag/v0.5.0
# and download nvim.appimage (Unstable only 0.5+ May be stable
# in the near future and installable via apt-get
# then run ln -s <full path to nvim.appimage> /usr/local/bin/nvim

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 || return ; pwd -P )"
ln -sf "$SCRIPTPATH/.config" "$HOME/.config"
ln -sf "$SCRIPTPATH/.zshrc" "$HOME/.zshrc"

# Get necessary packages
sudo apt-get update && sudo apt-get upgrade
sudo apt install -y zsh build-essential curl curl default-jre fuse g++ gcc libbz2-dev libffi-dev liblzma-dev libncurses5-dev libncursesw5-dev libreadline-dev libsqlite3-dev libssl-dev llvm make man nodejs npm tk-dev scala shellcheck wget xz-utils zlib1g-dev zplug 

# Install pyenv
#curl -L https://raw.githubusercontent.com/pyenv/pyenv-installer/master/bin/pyenv-installer | bash
pyenv install -s 3.9.7

# shellcheck source=/dev/null
source "$HOME/.zshrc"

# Install GO
#curl -O https://golang.org/dl/go1.17.1.linux-amd64.tar.gz
SUM=dab7d9c34361dc21ec237d584590d72500652e7c909bf082758fb63064fca0ef
SUM_STATUS=$(echo "$SUM  go1.17.1.linux-amd64.tar.gz" | sha256sum -c | awk '{print $2}')
if [ "$SUM_STATUS" != "OK" ]; then
	exit 1 
fi
tar xvf go1.17.1.linux-amd64.tar.gz
#rm go1.17.1.linux-amd64.tar.gz
sudo chown -R root:root ./go
sudo mv go /usr/local/go

Install GO packages
go install github.com/mattn/efm-langserver@master
go install mvdan.cc/sh/v3/cmd/shfmt@latest
